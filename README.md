# HP 800 G2 Mini - Ventura OpenCore Hackintosh

- OpenCore 0.9.1
- Intel i5-6500T

## Installation

- Extract `EFI/OC/Resources`:

  ```
  cd EFI/OC/Resources/; unzip Resources.zip; rm Resources.zip; cd -
  ```

- Update SMBios with your generated ID's. (MLB, ROM, SystemSerialNumber, SystemUUID)

## Notes

- Updated from Monterey to Ventura (not sure if direct install would work).
- My front USB C port doesn't work, so I cannot confirm if it _actually_ works.
- Sleep not tested (and I assume completely broken like all HD 530 iGPU's).
- I _could not_ use a DP -> HDMI adapter like I was on Monterey. I needed to use a direct cable.
